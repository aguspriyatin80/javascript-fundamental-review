/*
1. THE IF STATEMENT

Example 1:
var myNum1 = 7;
var myNum2 = 10;
if (myNum1 < myNum2) {
   alert("JavaScript is easy to learn.");
}
// Result: Javascript is easy to learn.

Example 2:
var myNum1 = 7;
var myNum2 = 10;
if (myNum1 > myNum2) {
   alert("JavaScript is easy to learn.");
}
// this code will nothing output

Note: Note that if is in lowercase letters. Uppercase letters (If or IF) will generate an error.

2. ELSE STATEMENT

Example:
var myNum1 = 7;
var myNum2 = 10;
if (myNum1 > myNum2) {
   alert("This is my first condition");
}
else {
   alert("This is my second condition");
}

// Result : This is my second condition

3. ELSE IF STATEMENT

Example:
var course = 3;
if (course == 1) {
   document.write("<h1>HTML Tutorial</h1>");
} else if (course == 2) {
   document.write("<h1>CSS Tutorial</h1>");
} else {
   document.write("<h1>JavaScript Tutorial</h1>");
}

// Result : Javascript Tutorial

4. SWITCH STATEMENT

Example 1:
var day = 2;
switch (day) {
  case 1:
    document.write("Monday");
    break;
  case 2:
    document.write("Tuesday");
    break;
  case 3:
    document.write("Wednesday");
    break;
  default:
    document.write("Another day");
}

// Result: "Tuesday"

Example 2:
var day_of_week = 1;
switch (day_of_week) {
  case 1:
  case 2:
  case 3:
  case 4:
  case 5:
    document.write("Working Days");
    break;
;
  case 6:
    document.write("Saturday");
    break;
;
  default:
    document.write("Today is Sunday");
    break;
}
// Result: 
Note: 
You can have as many case statements as needed.
The default block can be omitted, if there is no need to handle the case when no match is found.


5. FOR LOOP

// For Loop
Example:
for (i=1; i<=5; i++) {
   document.write(i + "<br />");
}
// Result :
1
2
3
4
5

Note: You can have multiple nested for loops.

6. WHILE LOOP

Example:
var i=0;
while (i<=5) {
   document.write(i + "<br />");
   i++;
}

// Result : 
0
1
2
3
4
5

Note: 
Be careful writing conditions. If a condition is always true, the loop will run forever.
Make sure that the condition in a while loop eventually becomes false.

7. DO WHILE LOOP

Example:
var i=20;
do {  
  document.write(i + "<br />");
  i++;  
}
while (i<=25); 

Note:
To understand the different of while loop is declaring var i=26 or bigger.
The loop will always be executed at least once, even if the condition is false, 
because the code block is executed before the condition is tested.

8. BREAK AND CONTINUE

// BREAK
Example:
for (i = 0; i <= 10; i++) {
   if (i == 5) {
     break; 
   }
   console.log(i + "<br />");
}

// Result : 
0
1
2
3
4

Note:
You can use the return keyword to return some value immediately from the loop inside of a function. This will also break the loop.

// CONTINUE
Example:
for (i = 0; i <= 10; i++) {
   if (i == 5) {
      continue; 
   }
   document.write(i + "<br />");
}

*/