/*

1. JAVASCRIPT ARRAYS

Arrays store multiple values in a single variable.

To store three course names, you need three variables.var course1 ="HTML"; 
var course2 ="CSS"; 
var course3 ="JS"; 
But what if you had 500 courses? The solution is an array. 
var courses = new Array("HTML", "CSS", "JS"); 
This syntax declares an array named courses, which stores three values, or elements.

2. ACCESSING AN ARRAY

You refer to an array element by referring to the index number written in square brackets.
This statement accesses the value of the first element in courses and changes the value of the second element. 

var courses = new Array("HTML", "CSS", "JS"); 
var course = courses[0]; // HTML
courses[1] = "C++"; //Changes the second element 

Note: [0] is the first element in an array. [1] is the second. Array indexes start with 0.

3. OTHER WAYS TO CREATE ARRAYS

You can also declare an array, tell it the number of elements it will store, and add the elements later.

var courses = new Array(3);
courses[0] = "HTML";
courses[1] = "CSS";
courses[2] = "JS";

Note:
An array is a special type of object.
An array uses numbers to access its elements, and an object uses names to access its members. 


JavaScript arrays are dynamic, so you can declare an array and not pass any arguments with the Array() constructor. 
You can then add the elements dynamically.

var courses = new Array();
courses[0] = "HTML";
courses[1] = "CSS";
courses[2] = "JS";
courses[3] = "C++";

// ARRAY LITERAL

For greater simplicity, readability, and execution speed, you can also declare arrays using the array literal syntax.

var courses = ["HTML", "CSS", "JS"]; 

This results in the same array as the one created with the new Array() syntax.
You can access and modify the elements of the array using the index number, as you did before.
The array literal syntax is the recommended way to declare arrays

4. ARRAY PROPERTIES AND METHODS

// LENGTH PROPERTY

var courses = ["HTML", "CSS", "JS"];
document.write(courses.length);
//Outputs 3

Note:
The length property is always one more than the highest array index.
If the array is empty, the length property returns 0.

// COMBINING ARRAYS

JavaScript's concat() method allows you to join arrays and create an entirely new array.
Example:
var c1 = ["HTML", "CSS"];
var c2 = ["JS", "C++"];
var courses = c1.concat(c2);

The courses array that results contains 4 elements (HTML, CSS, JS, C++).
The concat operation does not affect the c1 and c2 arrays - it returns the resulting concatenation as a new array.

5. MATH OBJECTS

document.write(Math.PI);
//Outputs 3.141592653589793


Math has no constructor. There's no need to create a Math object first.

For example, the following will calculate the square root of a number.

var number = Math.sqrt(4); 
document.write(number);
//Outputs 2

Note:
To get a random number between 1-10, use Math.random(), which gives you a number between 0-1. 
Then multiply the number by 10, and then take Math.ceil() from it: Math.ceil(Math.random() * 10).

Example:
var n = prompt("Enter a number", "");
var answer = Math.sqrt(n);
alert("The square root of " + n + " is " + answer);

6. DATE OBJECT

When a Date object is created, a number of methods make it possible to perform operations on it. 
Methods: getFullYear(), getMonth(), getDate(), getDay(), getHours(), getMinutes(), getSeconds(), getMiliseconds()

Example : 
var d = new Date();
var hours = d.getHours();
//hours is equal to the current hour

Let's create a program that prints the current time to the browser once every second.

function printTime() {
  var d = new Date();
  var hours = d.getHours();
  var mins = d.getMinutes();
  var secs = d.getSeconds();
  document.body.innerHTML = hours+":"+mins+":"+secs;
}
setInterval(printTime, 1000);


*/