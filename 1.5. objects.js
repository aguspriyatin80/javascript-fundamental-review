/*
1. JAVASCRIPT OBJECTS
JavaScript variables are containers for data values. Objects are variables too, but they can contain many values.

Think of an object as a list of values that are written as name:value pairs, with the names and the values separated by colons.
Example:var person = {
 name: "John", age: 31, 
 favColor: "green", height: 183
};

Note: JavaScript objects are containers for named values.

2. OBJECT PROPERTIES

This example demonstrates how to access the age of our person object. 
var person = {
 name: "John", age: 31, 
 favColor: "green", height: 183
};
var x = person.age;
var y = person['age'];

JavaScript's built-in length property is used to count the number of characters in a property or string.

var course = {name: "JS", lessons: 41};
document.write(course.name.length);
//Outputs 2

Note: Objects are one of the core concepts in JavaScript.

3. OBJECT METHODS

As you already know, document.write() outputs data. The write() function is actually a method of the document object.
document.write("This is some text");

Note : Methods are functions that are stored as object properties.

4. OBJECT CONSTRUCTOR

In the previous lesson, we created an object using the object literal (or initializer) syntax.
var person = {
name: "John", age: 42, favColor: "green"
};

This allows you to create only a single object.
Sometimes, we need to set an "object type" that can be used to create a number of objects of a single type.
The standard way to create an "object type" is to use an object constructor function. 

function person(name, age, color) {
  this.name = name;
  this.age = age;
  this.favColor = color;
}

The above function (person) is an object constructor, which takes parameters and assigns them to the object properties. 

Note :
The this keyword refers to the current object.
Note that this is not a variable. It is a keyword, and its value cannot be changed.

5. CREATING OBJECTS

Once you have an object constructor, you can use the new keyword to create new objects of the same type.

var p1 = new person("John", 42, "green");
var p2 = new person("Amy", 21, "red");

document.write(p1.age); // Outputs 42
document.write(p2.name); // Outputs "Amy"

Note : p1 and p2 are now objects of the person type. Their properties are assigned to the corresponding values.

Consider the following example :

function person (name, age) {
  this.name = name;
  this.age = age;
}
var John = new person("John", 25);
var James = new person("James", 21);

Access the object's properties by using the dot syntax, as you did before. 
John.name // John
James.age // 21

Note : Understanding the creation of objects is essential.

6. OBJECT INITIALIZATIONS

Use the object literal or initializer syntax to create single objects.
var John = {name: "John", age: 25};
var James = {name: "James", age: 21};

Note: 
Objects consist of properties, which are used to describe an object. 
Values of object properties can either contain primitive data types or other objects.

7. ADDING METHODS

Methods are functions that are stored as object properties. 

A method is a function, belonging to an object. It can be referenced using the 'this' keyword.
The 'this' keyword is used as a reference to the current object, meaning that you can access the objects properties and methods using it.

Defining methods is done inside the constructor function.
For Example:

function person(name, age) {
  this.name = name;  
  this.age = age;
  this.changeName = function (name) {
    this.name = name;
  }
}

var p = new person("David", 21);
p.changeName("John");
//Now p.name equals to "John"

You can also define the function outside of the constructor function and associate it with the object.

function person(name, age) {
  this.name= name;  
  this.age = age;
  this.yearOfBirth = bornYear;
}
function bornYear() {
  return 2016 - this.age;
}

Note : it's not necessary to write the function's parentheses when assigning it to an object.

Call the method as usual.

function person(name, age) {
  this.name= name;  
  this.age = age;
  this.yearOfBirth = bornYear;
}
function bornYear() {
  return 2016 - this.age;
}

var p = new person("A", 22);
document.write(p.yearOfBirth());
// Outputs 1994

Note : Call the method by the property name you specified in the constructor function, rather than the function name.


*/