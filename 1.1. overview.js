/*

1. INTRODUCTION
JavaScript is one of the most popular programming languages on earth and is used to add interactivity to webpages, process data, as well as create various applications (mobile apps, desktop apps, games, and more)
Learning the fundamentals of a language will enable you to create the program you desire, whether client-side or server-side.
You can use JavaScript to create interactive web elements


2. CREATING YOUR FIRST JAVASCRIPT

Let's start with adding JavaScript to a webpage.
JavaScript on the web lives inside the HTML document.
In HTML, JavaScript code must be inserted between <script> and </script> tags: 

<html>
    <head>
        <title>Your first javascript</title>
        <script>
            your javascript code here
        </script>
    </head>
    <body>
    </body>
</html>

Remember that the script, which is placed in the head section, will be executed before the <body> is rendered. If you want to get elements in the body, it's a good idea to place your script at the end of the body tag.

<html>
    <head>
        <title>Your first javascript</title>
    </head>
    <body>
        <script>
            your javascript code here
        </script>
    </body>
</html>

This code will give output Hello World in your browser.
<html>
   <head> </head>
   <body>
     <script>
       document.write("Hello World!"); -> this will output in the body
       console.log("Hello World!"); -> this will give output in the console
     </script>
   </body>
</html> 

3. EXTERNAL JAVASCRIPT

<html>
   <head>
     <title> </title>
     <script src="demo.js"></script>
   </head>
   <body>
   </body>
</html>

Placing a JavaScript in an external file has the following advantages:
- It separates HTML and code.
- It makes HTML and JavaScript easier to read and maintain.
- Cached JavaScript files can speed up page loads.

4. COMMENT IN JAVASCRIPT

// this is for single line comment
/* this 
    for 
    multiple 
    line 
    comment     
*/