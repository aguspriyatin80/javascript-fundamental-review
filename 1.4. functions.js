/*

1. USER DEFINED FUNCTIONS

A JavaScript function is a block of code designed to perform a particular task.
The main advantages of using functions:
Code reuse: Define the code once, and use it many times.
Use the same code many times with different arguments, to produce different results.
A JavaScript function is executed when "something" invokes, or calls, it.

Syntax:
function name() {    
  //code to be executed
}
Note: Function names can contain letters, digits, underscores, and dollar signs (same rules as variables).

// Calling a Function
function myFunction() {
  alert("Calling a Function!");
}

myFunction();
//Alerts "Calling a Function!"

Note : 
Always remember to end the statement with a semicolon after calling the function.
You can also call a function using this syntax: myFunction.call(). 
The difference is that when calling in this way, you're passing the 'this' keyword to a function. You'll learn about it later.

2. FUNCTION PARAMETERS

After defining the parameters, you can use them inside the function.
Example: 
function sayHello(name) {
   alert("Hi, " + name);
}

sayHello("David");
//Alerts "Hi, David"

This function takes in one parameter, which is called name. 
When calling the function, provide the parameter's value (argument) inside the parentheses. 

Note:
Function arguments are the real values passed to (and received by) the function.
You can define a single function, and pass different parameter values (arguments) to it.

function sayHello(name) {
   alert("Hi, " + name);
}
sayHello("David");
sayHello("Sarah");
sayHello("John");

3. MULTIPLE PARAMETERS

Example:
function sayHello(name, age) {
  document.write( name + " is " + age + " years old.");
}

sayHello("John", 20)
//Outputs "John is 20 years old."


4. FUNCTION RETURN

A function can have an optional return statement. It is used to return a value from the function.

This statement is useful when making calculations that require a result.
When JavaScript reaches a return statement, the function stops executing.

Example 1:
function myFunction(a, b) {
   return a * b;
}

var x = myFunction(5, 6); 
// Return value will end up in x
// x equals 30

Example 2:
function addNumbers(a, b) {
   var c = a+b;
   return c;
}
document.write( addNumbers(40, 2) );
//Outputs 42
Note: If you do not return anything from a function, it will return undefined.

5. ALERT, PROMPT, CONFIRM

// ALERT
alert("Hello\nHow are you?");

// PROMPT BOX
var user = prompt("Please enter your name");
alert(user);

// CONFIRM BOX
var result = confirm("Do you really want to leave this page?");
if (result == true) {
  alert("Thanks for visiting");
}
else {
  alert("Thanks for staying with us");
}
Note:
Do not overuse this method, because it also prevents the user from accessing other parts of the page until the box is closed.

*/