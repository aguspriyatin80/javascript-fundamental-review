/*
1. VARIABLES

Variables are containers for storing data values. The value of a variable can change throughout the program.
Use the var keyword to declare a variable:
var x = 10;
In the example above, the value 10 is assigned to the variable x.
JavaScript is case sensitive. For example, the variables lastName and lastname, are two different variables.

// The Equal Sign

In JavaScript, the equal sign (=) is called the "assignment" operator, rather than an "equal to" operator.
For example, x = y will assign the value of y to x.
A variable can be declared without a value. The value might require some calculation, something that will be provided later, like user input.
A variable declared without a value will have the value undefined.

// Using Variables

Let's assign a value to a variable and output it to the browser.
var x = 100;
document.write(x);

Using variables is useful in many ways. You might have a thousand lines of code that may include the variable x. 
When you change the value of x one time, it will automatically be changed in all places where you used it.
Every written "instruction" is called a statement. JavaScript statements are separated by semicolons.

// Naming Variables

JavaScript variable names are case-sensitive.
In the example below we changed x to uppercase: 

This code will not result in any output, as x and X are two different variables.
var x = 100;
document.write(X);

Naming rules:
- The first character must be a letter, an underscore (_), or a dollar sign ($). 
  Subsequent characters may be letters, digits, underscores, or dollar signs.
- Numbers are not allowed as the first character.
- Variable names cannot include a mathematical or logical operator in the name. For instance, 2*something or this+that;
- JavaScript names must not contain spaces.

There are some other rules to follow when naming your JavaScript variables:

- You must not use any special symbols, like my#num, num%, etc.
- Be sure that you do not use any of the following JavaScript reserved words. 

2. DATA TYPES

The term data type refers to the types of values with which a program can work. JavaScript variables can hold many data types, 
such as numbers, strings, arrays, and more.
Unlike many other programming languages, JavaScript does not define different types of numbers, like integers, short, long, floating-point, etc.

2.1 Numbers
JavaScript numbers can be written with or without decimals.
var num = 42; // A number without decimals

JavaScript numbers can be written with or without decimals.
var num = 42; // A number without decimals

Note:
This variable can be easily changed to other types by assigning to it any other data type value, like num = 'some random string'.

// Floating Point Numbers
<script>
  var price = 55.55;
  document.write(price);
</script>

2.1 Strings

JavaScript strings are used for storing and manipulating text.
A string can be any text that appears within quotes. You can use single or double quotes.
var name = 'John';
var text = "My name is John Smith";

You can use quotes inside a string, as long as they don't match the quotes surrounding the string.
var text = "My name is 'John' ";

Note:
You can get double quotes inside of double quotes using the escape character like this: \" or \' inside of single quotes.

// Escape Characters / backslash (\)
Example:
var sayHello = 'Hello world! \'I am a JavaScript programmer.\' ';
document.write(sayHello);

Note:
If you begin a string with a single quote, then you should also end it with a single quote. The same rule applies to double quotes. 
Otherwise, JavaScript will become confused.

2.2 Booleans

In JavaScript Boolean, you can have one of two values, either true or false.
These are useful when you need a data type that can only have one of two values, such as Yes/No, On/Off, True/False.

Example: 
var isActive = true; 
var isHoliday = false;

Note:
The Boolean value of 0 (zero), null, undefined, empty string is false.
Everything with a "real" value is true.

3. MATH OPERATORS

Arithmatics Operator : +, -, *, /, %, ++, --

Example:
var x = 10 + 5;
document.write(x); // Outputs 15

Note:
You can get the result of a string expression using the eval() function, which takes a string expression argument like eval("10 * 20 + 8") 
and returns the result. If the argument is empty, it returns undefined.

// Multiplication
Example:
var x = 10 * 5;
document.write(x); // Outputs 50

Note:
10 * '5' or '10' * '5' gives the same result. Multiplying a number with string values like 'binar' * 5 returns NaN (Not a Number).

// Division
Example:
var x = 100 / 5;
document.write(x); // Outputs 20

Note:
Remember to handle cases where there could be a division by 0.

// Modulus
Modulus (%) operator returns the division remainder (what is left over).
Example:
var myVariable = 26 % 6; // myVariable equals 2

Note:
In JavaScript, the modulus operator is used not only on integers, but also on floating point numbers.


// Increment & Decrement
Example var++:
var a = 0, b= 10;
var a = b++; // Result a = 10, b = 11

Example ++var:
var a = 0, b= 10;
var a = ++b; // Result a = 11, b = 11

Example var--:
var a = 0, b= 10;
var a = b--; // Result a = 10, b = 9

Example --var:
var a = 0, b= 10;
var a = --b; // Result a = 9, b = 9

Note:
As in school mathematics, you can change the order of the arithmetic operations by using parentheses.
Example: var x = (100 + 50) * 3;

4. ASSIGNMENT OPERATORS

symbols: =, +=, -=, *=, /=, %=
x = y is equivalent to x = y
x += y is equivalent to x = x + y
x -= y is equivalent to x = x - y
x *= y is equivalent to x = x * y
x /= y is equivalent to x = x / y
x %= y is equivalent to x = x % y

Note:
You can use multiple assignment operators in one line, such as x -= y += 9.

5. COMPARISON OPERATORS

Symbols: ==, ===, !=, !==, >, >=, <, <=
Comparison operators are used in logical statements to determine equality or difference between variables or values. They return true or false.
The equal to (==) operator checks whether the operands' values are equal.
var num = 10; 
// num == 8 will return false

Note:
You can check all types of data; comparison operators always return true or false.
When using operators, be sure that the arguments are of the same data type; numbers should be compared with numbers, strings with strings, and so on.

6. LOGICAL OPERATORS

Symbols: &&(AND), ||(OR), !(NOT)

Example:
(4 > 2) && (10 < 15)

For this expression to be true, both conditions must be true.
- The first condition determines whether 4 is greater than 2, which is true.
- The second condition determines whether 10 is less than 15, which is also true.
Based on these results, the whole expression is found to be true.

// Ternary Operator
Syntax:
variable = (condition) ? value1: value2 

Example:
var isAdult = (age < 18) ? "Too young": "Old enough";

Note: 
You can check all types of data; comparison operators always return true or false.
Logical operators allow you to connect as many expressions as you wish.


7. STRING OPERATORS

The most useful operator for strings is concatenation, represented by the + sign.
Concatenation can be used to build strings by joining together multiple strings, or by joining strings with other types:
var mystring1 = "I am learning ";
var mystring2 = "JavaScript with Glints.";
document.write(mystring1 + mystring2); // I am learning Javascript with Glints

Note:
Numbers in quotes are treated as strings: "42" is not the number 42, it is a string that includes two characters, 4 and 2.
*/